# Demo-project to start publishing into Kafka from PHP

By symfony tutorial https://symfony.com/doc/current/messenger.html

## Steps

1. [Init project](https://symfony.com/doc/4.2/best_practices/creating-the-project.html):
    ```shell
    mkdir php-kafka-demo
    cd php-kafka-demo
    composer create-project symfony/skeleton .
    ```

2. Git init:
    ```shell
    git init
    git add -A
    git commit -m"Init project"
    ```

3. Build container and start server

    For full reproducibility and also for greatly simplify process of install kafka support we will use `podman` (`docker`) container.
    Build for that present in project [php-kafka.container](https://github.com/Hubbitus/container-php-apache-kafka).

    To run our Symfony project just use provided [docker-compose.yml]() file:
    ```shell
    podman-compose up
    ```

4. To allow work [routes](https://symfony.com/doc/current/bundles/SensioFrameworkExtraBundle/annotations/routing.html) for the project run under Apache (`podman exec -it php-apache-kafka <cmd>`)

    ```shell
    composer config extra.symfony.allow-contrib true
    composer req symfony/apache-pack
    ```

5. Add `messanger` bundle:

    Start by [tutorial](https://symfony.com/doc/current/messenger.html) and [this one](https://symfony.com/doc/current/components/messenger.html):
    ```shell
    composer require symfony/messenger
    
    What's next? 
      * You're ready to use the Messenger component. You can define your own message buses
        or start using the default one right now by injecting the message_bus service
        or type-hinting Symfony\Component\Messenger\MessageBusInterface in your code.
    
      * To send messages to a transport and handle them asynchronously:
    
        1. Uncomment the MESSENGER_TRANSPORT_DSN env var in .env
           and framework.messenger.transports.async in config/packages/messenger.yaml;
        2. Route your message classes to the async transport in config/packages/messenger.yaml.
    
      * Read the documentation at https://symfony.com/doc/current/messenger.html
    ```

6. WEB-call endpoints (see DefaultController):
   - http://localhost:8087/test-message - very simple message handling. Just by echo that. See handling in DemoNotificationHandler.php ()
   - http://localhost:8087/test-message-kafka-direct - DIRECT kafka send (without dispatching, routes etc.). For faster debugging!

    At that point we should be able to DIRECT (without async) produce message in kafka (plain string, without auth)

7. [Continue](https://symfony.com/doc/current/messenger.html#transports-async-queued-messages) to dispatching message and use [Enqueue transport](https://github.com/sroze/messenger-enqueue-transport) - https://php-enqueue.github.io/transport/kafka/

    ```shell
    podman exec -it php-apache-kafka composer require sroze/messenger-enqueue-transport enqueue/rdkafka
    ```

    Also refer that simple [step-by-step guide on SO](https://stackoverflow.com/questions/58317692/symfony-messenger-with-apache-kafka-as-queue-transport).

    We need configure that in:
      - config/packages/messenger.yaml
      - config/packages/enqueue.yaml
      - .env
      - config/bundles.php

    See commit in history with "readme 7" item.

    **7.1.** Configure also filesystem transport in the same way. Also configure failed queue

8. Lets add AVRO support.
    ```shell
    podman exec -it php-apache-kafka composer require jaumo/avro
    podman exec -it php-apache-kafka composer require doctrine/cache=1.5
    podman exec -it php-apache-kafka composer require amphp/http-client
    ```
    > *Note* doctrine/cache version greater than >1.5 probably will not work.
    
    > *Note2*. https://github.com/flix-tech/avro-serde-php may be possible good alternative!
    
    Most of the values hardcoded now for the simplicity.
    But you may already see topic with schema in AVRO format in sandbox: https://sandbox.datahub.epam.com/kafka-manager/topics/test.php.2.enqueue.avro/browser

9. Next we will try to generate our AVRO schema by object on the fly!
    ```shell
    podman exec -it php-apache-kafka composer require php-kafka/php-avro-schema-generator "^2.2"
    podman exec -it php-apache-kafka composer require ocramius/generated-hydrator "^4.2.0"
    ```

10. GSSAPI Kerberos authentication!
    Probable the last part is to connect over Kerberos security!
    Please note, provided docker container in [docker-compose.yml](@DEV/docker-compose.yml) already has librdkafka build with GSSAPI. See repository https://github.com/Hubbitus/container-php-apache-kafka

    So, we need only properly configure it:
    1. First we need generate keytab file for the account. Please run [@DEV/keytab.regenerate](@DEV/keytab.regenerate). In that file also placed detailed instructions what it is and how to use.
    2. Then that file needs to be configured for use. See all settings in the file [config/packages/enqueue.yaml](config/packages/enqueue.yaml), for the `enqueue_kafka_kerberos` transport.

    Some implementation notes and highlights:
      - Provided implementation also have simple async errors handling in the `KafkaErrorsHandler` class
      - To make it more "serious" Symfony should get resolved issue [[Messenger] Send message to Failure Transport if async Sender fails #35521](https://github.com/symfony/symfony/issues/35521)
      - That implementation already has such experiment included but disabled! See class [SendFailedAsyncMessageToFailureTransportMiddleware](). Despite it works, there is some troubles then use failed messages. See my bugreport [ Filesystem transport: The "failed" receiver does not support listing or showing specific messages #1226](https://github.com/php-enqueue/enqueue-dev/issues/1226). See very simple logging of the failed send to kafka messages in `App\Core\Messenger\KafkaErrorsHandler::delivery_report_callback`
