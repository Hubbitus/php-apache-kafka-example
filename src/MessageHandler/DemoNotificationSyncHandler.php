<?php
namespace App\MessageHandler;

use App\DAO\DemoNotification;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class DemoNotificationSyncHandler implements MessageHandlerInterface {
    public function __construct(){
        echo 'DemoNotificationHandler constructor! Should be called once.';
    }

    public function __invoke(DemoNotification $message) {
        // ... do some work - like sending an SMS message!
        echo "<p>SYNC DemoNotificationHandler: message send: " . $message->getContent() . '</p>';
    }
}
