<?php

declare(strict_types=1);

namespace App\Core\Messenger;

use App\Core\Messenger\Serializing\AvroSerializer;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Exception\TransportException;
use Symfony\Component\Messenger\Stamp\ErrorDetailsStamp;

class KafkaErrorsHandler {
    public static int $successCounter = 0;

    /**
     * General handler for the kafka (fired with enable.idempotence=true).
     * The error callback is used by librdkafka to signal critical errors back to the application.
     * @link https://arnaud.le-blanc.net/php-rdkafka-doc/phpdoc/rdkafka-conf.seterrorcb.html
     * @link https://github.com/Steveb-p/enqueue-dev/blob/05a5073217d4c784dc966155b302b244325d9518/pkg/rdkafka/RdKafkaConnectionFactory.php see also
     * @param \RdKafka\Producer $kafka
     * @param int $err
     * @param string $reason
     * @return mixed
     */
    public static function error_callback(\RdKafka\Producer $kafka, int $err, string $reason){
        throw new TransportException(sprintf("[error_callback] Kafka error: %s (reason: %s)\n", \rd_kafka_err2str($err), $reason));
    }

    /**
     * The delivery report callback will be called once for each message accepted by RdKafka\ProducerTopic::produce() with err set to indicate the result of the produce request.
     * @link https://arnaud.le-blanc.net/php-rdkafka-doc/phpdoc/rdkafka-conf.setdrmsgcb.html
     * @link https://github.com/Steveb-p/enqueue-dev/blob/05a5073217d4c784dc966155b302b244325d9518/pkg/rdkafka/RdKafkaConnectionFactory.php see also
     * @param \RdKafka\Producer $kafka
     * @param \RdKafka\Message $message
     * @return void
     * @throws \Throwable
     */
    public static function delivery_report_callback(\RdKafka\Producer $kafka, \RdKafka\Message $message){
        if ($message->err) {
            $e = new TransportException(sprintf("[delivery_report_callback] Kafka delivery message error: %s:. Message topic: %s\n", \rd_kafka_err2str($message->err), $message->topic_name));
            $obj = (new AvroSerializer())->binaryToObject($message->payload);
            $envelope = new Envelope($obj, [ErrorDetailsStamp::create($e)]);
            // @TODO That is very simple failed messages logging!
            // Please look at my bugreport https://github.com/sroze/messenger-enqueue-transport/issues/110
            file_put_contents(__DIR__ . '/../../../var/kafka-failed-send.messages', '=====[' . strftime('%F %T %Z ') . hrtime()[1] .']=====: ' . var_export(AvroSerializer::objectToArray($envelope), true) . "\n\n", FILE_APPEND | LOCK_EX);
            throw $e;
        }
        self::$successCounter++;
    }
}
