<?php
namespace App\Core\Messenger\Serializing;

use Amp\Dns\DnsException;
use Doctrine\Common\Cache\ArrayCache;
use Enqueue\RdKafka\Serializer;
use Enqueue\RdKafka\RdKafkaMessage;

use GeneratedHydrator\Configuration;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Exception\TransportException;
use Symfony\Component\Messenger\Transport\Serialization\PhpSerializer;
use Symfony\Component\Messenger\Exception\MessageDecodingFailedException;

use \Avro\Serde;
use \Amp\Http\Client\HttpClientBuilder;
use Avro\SchemaRegistry\AmpClient;
use Avro\SchemaRegistry\CachedClient;
use Avro\SchemaRegistry\DefaultSerializer;
use Avro\SchemaRegistry\Options;

use PhpKafka\PhpAvroSchemaGenerator\Registry\ClassRegistry;
use PhpKafka\PhpAvroSchemaGenerator\Generator\SchemaGenerator;

use Laminas\Hydrator\ArraySerializableHydrator;

/**
 * @see https://github.com/php-enqueue/enqueue-dev/blob/master/docs/transport/kafka.md#serialize-message
 */
class AvroSerializer implements Serializer {
    /**
     * @throws TransportException|\Throwable
     */
    public function toMessage($string): RdKafkaMessage {
        $object = $this->binaryToObject($string);

        return new RdKafkaMessage((new PhpSerializer())->encode(new Envelope($object))['body']);
    }

    /**
     * @throws \Throwable
     */
    public function toString(RdKafkaMessage $message): string {
        try {
            // See encoding in Enqueue\MessengerAdapter\QueueInteropTransport.encodeMessage(Envelope $envelope)
            $envelope = (new PhpSerializer())->decode(array(
                'body' => $message->getBody(),
                'headers' => $message->getHeaders(),
                'properties' => $message->getProperties(),
            ));
        } catch (MessageDecodingFailedException $e) {
            throw $e;
        }

        return $this->encodeMessageWithSchemaRegistry($envelope->getMessage());
    }

    /**
     * @see https://gitlab.com/Jaumo/avro-php/-/blob/master/examples/message-serialization-with-schema-registry.php
     * @see https://stackoverflow.com/questions/62793575/producing-avro-message-using-php-enqueue/63592544#63592544 also may be helpful
     * @throws \Throwable
     */
    private function encodeMessageWithSchemaRegistry($object): string {
//        $schemaJson = <<<JSON
//{
//  "type": "record",
//  "name": "com.avro.Message",
//  "fields": [
//    {
//      "name": "content",
//      "type": "string"
//    }
//  ]
//}
//JSON;
//        $schema = Serde::parseSchema($schemaJson);

        $schemaJson = $this->generateSchema($object);
        // See FR https://github.com/php-kafka/php-avro-schema-generator/issues/33, now dirty hack to make all fields nullable:
        $schemaJson = preg_replace('/"type":\s*("(?:int|string|boolean|float)")/', '"type": ["null", $1], "default": null', $schemaJson);
//        $schemaJson = preg_replace('/"type":\s*("(?:int|boolean|float)")/', '"type": ["null", $1], "default": null', $schemaJson);
        $schema = Serde::parseSchema($schemaJson);
        $dataArray = $this->objectToArray($object);

        try {
            $promise = Serde::encodeMessageWithSchemaRegistry(
                $schema,
                $dataArray,
                'schema_subject1',
                $this->createSerializer()
            );

            return \Amp\Promise\wait($promise);
        }
        catch (DnsException $e){
            throw new TransportException('Kafka transport failed to resolve name of schema-registry!', null, $e);
        }
    }

    /**
     * @link https://github.com/php-kafka/php-avro-schema-generator#generating-schemas-from-classes by example
     * @param $data
     * @return string
     */
    private function generateSchema($data): string {
        $registry = (new ClassRegistry())
            ->addClassDirectory(__DIR__ . '/../../../DAO')
            ->load();
        $generator = new SchemaGenerator($registry, '');
        $schemas = $generator->generate();
        return $schemas[str_replace('\\', '.', get_class($data))];
    }

    /**
     * @return \Avro\SchemaRegistry\Serializer
     */
    private function createSerializer(): \Avro\SchemaRegistry\Serializer {
        return new DefaultSerializer(
            new CachedClient(
                new ArrayCache(),
                new AmpClient('http://schema-registry-sbox.epm-eco.projects.epam.com:8081', HttpClientBuilder::buildDefault())
            ),
            (new Options())->enableAutoSchemaRegistration()
        );
    }

    /**
     * @param string $string
     * @return mixed Actual object as it was before AVRO serialization. So, that may be arbitrary type!
     * @throws \Throwable
     */
    public function binaryToObject(string $string) {
        try {
            $promise = Serde::decodeMessageWithSchemaRegistry(
                $string, $this->createSerializer(),
            );

            $typedValue = \Amp\Promise\wait($promise);
        } catch (DnsException $e) {
            throw new TransportException('Kafka transport failed to resolve name of schema-registry!', null, $e);
        }

        // See https://github.com/Ocramius/GeneratedHydrator and solving problem: http://ocramius.github.io/blog/fast-php-object-to-array-conversion/
        $toClassName = str_replace('.', '\\', $typedValue->getSchema()->getNamespace() . '.' . $typedValue->getSchema()->getName());
        $hydratorClass = (new Configuration($toClassName))->createFactory()->getHydratorClass();
        $hydrator = new $hydratorClass();
        $object = new $toClassName();
        $hydrator->hydrate($typedValue->getValue(), $object);
        return $object;
    }

    /**
     * Convert any object into array for the easy serialisation.
     * Extracted all fields, despite visibility
     * @link https://github.com/Ocramius/GeneratedHydrator and solving problem: {@link http://ocramius.github.io/blog/fast-php-object-to-array-conversion/}
     * @param $object
     * @return array
     */
    public static function objectToArray($object): array {
        $hydratorClass = (new Configuration(get_class($object)))->createFactory()->getHydratorClass();
        $hydrator = new $hydratorClass();
        $dataArray = $hydrator->extract($object);
        return $dataArray;
    }
}
