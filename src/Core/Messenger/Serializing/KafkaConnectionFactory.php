<?php
namespace App\Core\Messenger\Serializing;

use App\Core\Messenger\Serializing\AvroSerializer;
use Enqueue\RdKafka\RdKafkaConnectionFactory;
use Interop\Queue\Context;

/**
* Workaround of configuration AvroSerializer serialization.
* @see https://github.com/sroze/messenger-enqueue-transport/issues/103
*/
class KafkaConnectionFactory extends RdKafkaConnectionFactory {
    public function createContext(): Context {
        $context = parent::createContext();
        $context->setSerializer(new AvroSerializer());

        return $context;
    }
}
