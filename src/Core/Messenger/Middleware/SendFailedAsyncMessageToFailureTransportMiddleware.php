<?php

declare(strict_types=1);

namespace App\Core\Messenger\Middleware;

use Amp\Dns\DnsException as DnsExceptionAlias;
use App\Core\Messenger\Stamp\FailedToBeSentToAsyncQueue;
use Symfony\Component\Messenger\Stamp\ErrorDetailsStamp;
use function count;
use Generator;
use function iterator_to_array;
use LogicException;
use Symfony\Component\ErrorHandler\Exception\FlattenException;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Exception\TransportException;
use Symfony\Component\Messenger\Middleware\MiddlewareInterface;
use Symfony\Component\Messenger\Middleware\StackInterface;
use Symfony\Component\Messenger\Stamp\DelayStamp;
use Symfony\Component\Messenger\Stamp\ReceivedStamp;
use Symfony\Component\Messenger\Stamp\RedeliveryStamp;
use Symfony\Component\Messenger\Stamp\SentToFailureTransportStamp;
use Symfony\Component\Messenger\Stamp\TransportMessageIdStamp;
use Symfony\Component\Messenger\Bridge\Doctrine\Transport\DoctrineReceivedStamp;
use Symfony\Component\Messenger\Transport\Sender\SenderInterface;
use Symfony\Component\Messenger\Transport\Sender\SendersLocatorInterface;

/**
 * @link https://github.com/symfony/symfony/issues/35521 by!
 */
final class SendFailedAsyncMessageToFailureTransportMiddleware implements MiddlewareInterface
{
    private const DELAY = 0;
    private const REDELIVERY_RETRY_COUNT = 0;

    private SenderInterface $failedTransportTransport;

    private SendersLocatorInterface $sendersLocator;

    public function __construct(SenderInterface $failedTransportSender, SendersLocatorInterface $sendersLocator)
    {
        $this->failedTransportTransport = $failedTransportSender;
        $this->sendersLocator = $sendersLocator;
    }

    public function handle(Envelope $envelope, StackInterface $stack): Envelope
    {
        $resultEnvelope = $envelope;

        try {
            if ($this->isReceivedFromFailureTransport($envelope)) {
                $resultEnvelope = $envelope
                    // to avoid handling message but instead use senders to redispatch to original transport, see SendMessageMiddleware
                    ->withoutAll(ReceivedStamp::class)
                    // process this message as a new and allow it to be sent to failure transport again in case of new errors
                    ->withoutAll(SentToFailureTransportStamp::class)
                    // to avoid redelivering to failed queue again. We need to process message by original sender (QueueInterop), see SendMessageMiddleware::getSenders
                    ->withoutAll(RedeliveryStamp::class)
                    // to avoid to entering into this `if` when the message is sent from Failure Queue to transport and consumed as normal
                    ->withoutAll(FailedToBeSentToAsyncQueue::class);
            }

            return $stack->next()->handle($resultEnvelope, $stack);
        } catch (TransportException $e) {
            if (!$this->isAlreadySentToFailedSender($resultEnvelope)) {
                // if the message came from failure doctrine transport, we should throw the error further to trigger the retry logic from worker
                if ($this->hasDoctrineReceivedStamp($resultEnvelope)) {
                    throw $e;
                }

                $originalReceiverName = $this->getOriginalReceiverName($resultEnvelope);

                // taken from built-in SendFailedMessageToFailureTransportListener
                $flattenedException = class_exists(FlattenException::class) ? FlattenException::createFromThrowable($e) : null;
//                $ErrorDetails = ErrorDetailsStamp::create($e);
                $newEnvelope = $resultEnvelope
                    ->withoutAll(ReceivedStamp::class)
                    ->withoutAll(TransportMessageIdStamp::class)
                    ->with(new SentToFailureTransportStamp($originalReceiverName))
                    ->with(new DelayStamp(self::DELAY))
                    ->with(new FailedToBeSentToAsyncQueue())
//                    ->with(new RedeliveryStamp(self::REDELIVERY_RETRY_COUNT, $e->getMessage(), $flattenedException));
                    ->with(new RedeliveryStamp(self::REDELIVERY_RETRY_COUNT))
                    ->with(ErrorDetailsStamp::create($e));

                $this->failedTransportTransport->send($newEnvelope);
            }
        }

        return $resultEnvelope;
    }

    /**
     * Returns `true` when the failed message is consumed from failure transport,
     * for example by running `bin/console messenger:failed:retry`
     *
     * @param Envelope $envelope
     *
     * @return bool
     */
    private function isReceivedFromFailureTransport(Envelope $envelope): bool
    {
        $failedToBeSentToAsyncQueueStamps = $envelope->all(FailedToBeSentToAsyncQueue::class);
        $receivedStamps = $envelope->all(ReceivedStamp::class);

        return count($failedToBeSentToAsyncQueueStamps) > 0 && count($receivedStamps) > 0;
    }

    private function isAlreadySentToFailedSender(Envelope $envelope): bool
    {
        return $envelope->last(SentToFailureTransportStamp::class) !== null;
    }

    private function hasDoctrineReceivedStamp(Envelope $envelope): bool
    {
        return $envelope->last(DoctrineReceivedStamp::class) !== null;
    }

    private function getOriginalReceiverName(Envelope $resultEnvelope): string
    {
        /** @var Generator<string, SenderInterface> $senders */
        $senders = $this->sendersLocator->getSenders($resultEnvelope);
        $sendersArray = iterator_to_array($senders);

        if (count($sendersArray) !== 1) {
            throw new LogicException('The number of aliases must be 1');
        }

        return array_key_first($sendersArray);
    }
}
