<?php

declare(strict_types=1);

namespace App\Core\Messenger\Stamp;

use Symfony\Component\Messenger\Stamp\NonSendableStampInterface;

class FailedToBeSentToAsyncQueue extends \Exception implements \Symfony\Component\Messenger\Stamp\NonSendableStampInterface {

}
