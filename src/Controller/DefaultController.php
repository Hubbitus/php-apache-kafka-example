<?php
// src/Controller/DefaultController.php
namespace App\Controller;

use App\DAO\DemoNotification;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Routing\Annotation\Route;

use Enqueue\RdKafka\RdKafkaConnectionFactory;

/**
 * @see https://symfony.com/doc/current/messenger.html#transports-async-queued-messages
 */
class DefaultController extends AbstractController {
    /**
    * @Route("/")
    */
    public function index(): Response {
        // Return random number: https://symfony.com/doc/current/page_creation.html
        $number = random_int(0, 100);
        return new Response(
            '<html><body>Lucky number: '.$number.'</body></html>'
        );
    }

    /**
    * @Route("/test-message")
    */
    public function testMessage(MessageBusInterface $bus): Response {
        // will cause the DemoNotificationHandler to be called
        $bus->dispatch(new DemoNotification('Look! I have created a message! [' . strftime('%F %T %Z ') . hrtime()[1] . ']'));

        // or use the shortcut
//        $this->dispatchMessage(new DemoNotification('Look! I created a message 2! [' . strftime('%F %T %Z ') . hrtime()[1] . ']'));
        return new Response(
            '<html><body>Dispatched!</body></html>'
        );
    }

    /**
    * @see https://github.com/sroze/messenger-enqueue-transport
    * @see https://github.com/php-enqueue/enqueue-dev/blob/master/docs/transport/kafka.md
    *
    * @Route("/test-message-kafka-direct")
    */
    public function testMessageKafkaDirect(): Response {
        $connectionFactory = new RdKafkaConnectionFactory([
            'global' => [
                'group.id' => uniqid('', true),
                'metadata.broker.list' => 'kafka-sbox.epm-eco.projects.epam.com:9092',
                'enable.auto.commit' => 'true',
                'debug' => 'all',
                'log_level' => '7'
            ],
            'topic' => [
                'auto.offset.reset' => 'beginning',
            ],
        ]);

        date_default_timezone_set('Europe/Moscow');

        $context = $connectionFactory->createContext();
        $message = $context->createMessage('Hello world [' . strftime('%F %T %Z ') . hrtime()[1] . ']!');
        $topic = $context->createTopic('test.php.0');
        $context->createProducer()->send($topic, $message);

        return new Response(
            '<html><body>Kafka message sent!</body></html>'
        );
    }
}
