<?php
namespace App\DAO;

use \DateTime;

class DemoNotification {
    private ?string $content;
    private ?int $dateTime;

    public function __construct(string $content = null, int $dateTime = null) {
        $this->content = $content;
        if (!is_null($dateTime)){
            $this->dateTime = $dateTime;
        }
        else {
            $this->dateTime = (new DateTime())->getTimestamp();
        }
    }

    public function getContent(): string {
        return $this->content;
    }
}
